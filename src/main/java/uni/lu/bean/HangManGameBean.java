package uni.lu.bean;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@ManagedBean
@SessionScoped
public class HangManGameBean implements Serializable {

    private List<String> mListOfWords;
    private int mSelectedWordPosition;
    private String reverseWord;
    private String reverseWordToDisplay;

    private String inputValue;
    private int nbAttempts;
    private boolean isGameFinished;

    /**
     * Method executed at startup
     */
    @PostConstruct
    public void init() {
        nbAttempts = 0;
        isGameFinished = false;

        // Add some words
        populateListOfWords();

        // Select randomly a word
        selectRandomWord();

        // Parse the selected word and reverse letters
        reverseWord();
    }

    /**
     * Method to read value from input Text
     *
     * @param value value from input Text
     */
    public void listenerGameInput(String value) {
        inputValue = value.toUpperCase();
    }

    /**
     * Method called from hangGame.xhtml. To guess word.
     */
    public void chooseActionGuess() {
        if (inputValue != null) {
            nbAttempts++;
            guessTheWord();
        }
    }

    /**
     * Method called from hangGame.xhtml. To restart game.
     */
    public void chooseActionRestart() {
        init();
    }

    /*
     * Define GETTERS - SETTERS to be read from JSF
     */
    public String getReverseWord() {
        return reverseWord;
    }

    public void setReverseWord(String reverseWord) {
        this.reverseWord = reverseWord;
    }

    public String getReverseWordToDisplay() {
        return reverseWordToDisplay;
    }

    public void setReverseWordToDisplay(String reverseWordToDisplay) {
        this.reverseWordToDisplay = reverseWordToDisplay;
    }

    public String getInputValue() {
        return inputValue;
    }

    public void setInputValue(String inputValue) {
        this.inputValue = inputValue.toUpperCase();
    }

    public int getNbAttempts() {
        return nbAttempts;
    }

    public void setNbAttempts(int nbAttempts) {
        this.nbAttempts = nbAttempts;
    }

    public boolean getIsGameFinished() {
        return isGameFinished;
    }

    public void setIsGameFinished(boolean isGameFinished) {
        this.isGameFinished = isGameFinished;
    }


    // ===========================================================================================================
    // ===========================================================================================================
    // ===========================================================================================================

    private void populateListOfWords() {
        mListOfWords = new ArrayList<>();

        mListOfWords.add("LEMON");
        mListOfWords.add("ORANGE");
        mListOfWords.add("CAR");
        mListOfWords.add("ICE-CREAM");
        mListOfWords.add("VOLKER");
        mListOfWords.add("TOTO");
        mListOfWords.add("JAPAN");
        mListOfWords.add("PASTA");
    }

    private void selectRandomWord() {
        mSelectedWordPosition = ThreadLocalRandom.current().nextInt(0, mListOfWords.size());
    }

    private void reverseWord() {
        StringBuilder reverseString = new StringBuilder();
        StringBuilder reverseStringForDisplay = new StringBuilder();
        String[] words = mListOfWords.get(mSelectedWordPosition).split(" ");

        for (String word : words) {
            String reverseWordToBuild = new StringBuilder(word).reverse().toString();
            reverseString.append(reverseWordToBuild).append(" ");
        }

        // Save the word
        reverseWord = reverseString.toString().trim();

        char[] chars = reverseWord.toCharArray();
        for (char c : chars) {
            if (((int) c <= 90) && ((int) c >= 65)) {
                reverseStringForDisplay.append("_").append(" ");
            } else {
                reverseStringForDisplay.append(c).append(" ");
            }
        }

        reverseWordToDisplay = reverseStringForDisplay.toString();
    }

    private void guessTheWord() {
        // Convert String to char
        boolean found = false;

        char[] chars = reverseWord.toCharArray();
        char cha = inputValue.charAt(0);

        StringBuilder sb = new StringBuilder(reverseWordToDisplay);
        for (int h = 0; h < chars.length; h++) {
            if (cha == chars[h]) {
                sb.setCharAt(h * 2, cha);
                found = true;
            }
        }

        // Update reverseWordToDisplay
        reverseWordToDisplay = sb.toString();

        if (found) {
            if (isGameFinished()) {
                isGameFinished = true;
            }
        }
    }

    private boolean isGameFinished() {
        boolean isFinished = true;

        if (reverseWordToDisplay.contains("_")) {
            isFinished = false;
        }

        return isFinished;
    }
}